 /*
                                        This is a buffered utility to regenerate a
                                        companion .shx file for a .shp file in the 
                                        same folder:  ./regenerate {.shp name}.  A
                                        pointer (unsigned char *buffer) stores the
                                        address to the buffer.  This allocation is 
                                        freed once the program completes.

                            Types:     The code skips shapefile types that are not 
                                       POINT or POLYGON since only these two types
                                       have been tested.  It would be quite simple
                                       to circumvent this restriction.  The switch
                                       statement imposing this restriction does so
                                       by sending other cases to default; but case
                                       5 would be the default for no restriction.

                            Acknowledgements: ESRI Shapefile Technical Description
                                              1997, 1998

                            Copyright:       All rights reserved (c) Kenn Lui 2024
 */
#include <arpa/inet.h>
#include <assert.h>
#include <fcntl.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define SHAPEFILE_STRICT_HEADER_SIZE 100
#define CHAR_BUF_SIZE INT_MAX/8
#define OFF_BUF_SIZE  INT_MAX/16
#define WDSZ_BUF_SIZE INT_MAX/16

unsigned char *buffer         = NULL;

static uint32_t  offset        =  50;    /* 50 words = 100 bytes */
static char shp_name [1024]    = {0};
static char shx_name [1024]    = {0};

typedef
union {
  unsigned char bytes [SHAPEFILE_STRICT_HEADER_SIZE];
  struct {
    int32_t  file_code    __attribute__((packed,aligned(4)));
    int32_t  unused [5]   __attribute__((packed,aligned(4)));
    int32_t  file_length  __attribute__((packed,aligned(4)));
    int32_t  version      __attribute__((packed,aligned(4)));
    int32_t  type         __attribute__((packed,aligned(4)));    
    double   x_min        __attribute__((packed,aligned(4)));
    double   y_min        __attribute__((packed,aligned(4)));
    double   x_max        __attribute__((packed,aligned(4)));
    double   y_max        __attribute__((packed,aligned(4)));
    double   z_min        __attribute__((packed,aligned(4)));
    double   z_max        __attribute__((packed,aligned(4)));
    double   m_min        __attribute__((packed,aligned(4)));
    double   m_max        __attribute__((packed,aligned(4)));
  };
} shapefile_header;

int main( int argc, char **argv ) {
  
  int  fdsource           =  0;
  int  fdtarget           =  0;
  int  bytes_read         =  0;
  int  bytes_written      =  0;
  int  num_rem_recs       =  0;
  int  record_count       =  0;
  int32_t shx_file_length =  0;       /* <===   Little endian in this code, */ 
  shapefile_header  shp_header;       /*     but big endian in the headers. */
  shapefile_header  shx_header;  
  int32_t  shp_offset    = 100;
  
  void      *cursor     = NULL;
  uint32_t  *intcursor  = NULL;

  int32_t  record_word_size;
  int32_t  record_byte_size;
  int32_t  local_record_byte_size;
  int32_t  dummy_int;
  int32_t  dummy_contents_page [4096];

  if( argc != 2 ) {
    (void)fprintf( stderr, "Syntax: ./regenerate {path to shapefile}\n" );
    return -1;
  }

  buffer = calloc( CHAR_BUF_SIZE, sizeof(unsigned char) );
  int32_t *record_offsets    = calloc( OFF_BUF_SIZE,  sizeof( int32_t ) );
  int32_t *record_word_sizes = calloc( WDSZ_BUF_SIZE, sizeof( int32_t ) );
  
  (void)memcpy( shp_name, argv[1], sizeof( shp_name ) );
  (void)memcpy( shx_name, argv[1], sizeof( shx_name ) );  
  fprintf( stderr, "%s\n", shp_name );
  shx_name[
    strlen( argv[1] ) - 1
  ] = 'x';
  
  fdsource = open(
    shp_name,
    O_RDONLY
  );
  if( fdsource == -1 ) {
    perror( "Failure - open shp" );
    free( buffer );
    free( record_offsets);
    free( record_word_sizes);          
    return -1;
  }

  fdtarget = open(
    shx_name,
    O_RDONLY
  );
  if( fdtarget != -1 ) {
    fprintf(
      stderr,
      "Existing .shx detected.  "
      "Abort.  Please remove '%s' "
      "and try again to regenerate.\n",
      shx_name
    );
    free( buffer );
    free( record_offsets);
    free( record_word_sizes);          
    return 0;
  }
  
  if( ( bytes_read = read(
    fdsource, &shp_header, SHAPEFILE_STRICT_HEADER_SIZE
  ) ) == -1 ) {
    perror( "Failure - read source into header" );
    (void)close( fdsource );
    free( buffer );
    free( record_offsets );
    free( record_word_sizes );
    return -1;
  }
  
  for( ; ; record_count += 1 ) {
    
    if( ( record_count == OFF_BUF_SIZE  ) ||
        ( record_count == WDSZ_BUF_SIZE ) ) {
      fprintf(
        stderr,
        "Failure - only room for %d records\n",
        record_count
      );
      (void)close( fdsource );
      free( buffer );
      free( record_offsets );
      free( record_word_sizes );      
      return -1;
    }
    
    if( ( bytes_read = read(
      fdsource, &dummy_int, 4
    ) ) == -1 ) {
      perror( "Failure - read source into dummy int" );
      (void)close( fdsource );
      free( buffer );
      free( record_offsets );
      free( record_word_sizes );      
      return -1;
    } else if(
      bytes_read == 0
    ) {
      break;
    } else {
      if( (int)ntohl( dummy_int ) != record_count + 1 ) {
        fprintf(
          stderr,
          "Failure - record number (%d) "
          "is not in expected sequence "
          "(%d)\n",
          (int)ntohl( dummy_int ),
          record_count + 1
        );
        (void)close( fdsource );
        free( buffer );
        free( record_offsets );
        free( record_word_sizes );      
        return -1;        
      }
    }

    record_offsets[record_count] = shp_offset;    
    shp_offset += 4;
    if( ( bytes_read = read(
      fdsource, &record_word_size, 4
    ) ) < 1 ) {
      perror( "Failure - read source into record_word_size" );
      (void)close( fdsource );
      free( buffer );
      free( record_offsets );
      free( record_word_sizes );
      return -1;
    }

    record_byte_size = ntohl( record_word_size );
    record_byte_size *= 2;
    local_record_byte_size = record_byte_size;
    shp_offset += 4;
    record_word_sizes[
      record_offsets[record_count]
    ] = record_word_size;
    
    while( 1 ) {
      if( ( bytes_read = read(
        fdsource,
        &dummy_contents_page,
        ( sizeof( dummy_contents_page ) < local_record_byte_size )
        ? sizeof( dummy_contents_page )
        : local_record_byte_size
      ) ) < 1 ) {
        perror( "Failure - read source into dummy contents page" );
        (void)close( fdsource );
        free( buffer );
        free( record_offsets );
        free( record_word_sizes);      
        return -1;
      }
      if(
        local_record_byte_size > sizeof( dummy_contents_page )
      ) {
        local_record_byte_size -= sizeof( dummy_contents_page );
      } else if(
        ( local_record_byte_size == bytes_read ) || !bytes_read 
      ) {
        break;
      }
    }
    
    fprintf(
      stderr,
      "Record %d byte size: %d\n",
      record_count + 1,
      record_byte_size
    );
    
    shp_offset += record_byte_size;
  }
  
  (void)close( fdsource );

  shx_header = shp_header;
  switch( shp_header.type ) {
    case 1 :
      num_rem_recs  = ntohl( shp_header.file_length );
      num_rem_recs -= offset;
      num_rem_recs /= 14;           /* each pair of point header plus record
                                     * requires 14 words = 8 bytes + 20 bytes 
                                     */
      shx_file_length = num_rem_recs;
      assert( num_rem_recs == record_count ); /* guard for debug */
      shx_file_length *= 4;         /* each index record requires 4 words =   */
      shx_file_length += offset;    /*        8 bytes = two 32 bit integers   */
      break;
    case 5 :
      shx_file_length = num_rem_recs = record_count;
      shx_file_length *= 4;
      shx_file_length += offset;
      break;
    default :
      fprintf( stderr, "Unable to parse shapefile.  Abort.\n" );
      free( buffer );
      free( record_offsets );
      free( record_word_sizes);
      return -1;
  }
  
  shx_header.file_length = htonl( shx_file_length );
  (void)memcpy(
    (void *)buffer,
    &shx_header,
    SHAPEFILE_STRICT_HEADER_SIZE
  );
  
  cursor = &buffer[SHAPEFILE_STRICT_HEADER_SIZE];
  intcursor = (uint32_t *)cursor;
  for( size_t i = 0; ; ++i ) {
    *intcursor = htonl( record_offsets[i]/2 );
    intcursor += 1;
    *intcursor = record_word_sizes[
      record_offsets[i]
    ];
    intcursor += 1;
    num_rem_recs -= 1;
    if( num_rem_recs < 1 ) {
    /*  *intcursor = EOF;  */
      break;
    } else if(
      record_offsets[i] > ntohl( shx_header.file_length )
    ) {
    /*  *intcursor = EOF;  */    
      break;
    } else if (
      i == (size_t)CHAR_BUF_SIZE - (size_t)SHAPEFILE_STRICT_HEADER_SIZE
    ) {
      fprintf( stderr, "Unable to accommodate next write in buffer.  Abort.\n" );
      free( buffer );
      free( record_offsets );
      free( record_word_sizes );
      return -1;
    }
  }

  fdtarget = open(
    shx_name,
    O_CREAT | O_WRONLY,
    0644
  );
  if( fdtarget == -1 ) {
    perror( "Failure - open shx" );
    free( buffer );
    free( record_offsets );
    free( record_word_sizes );
    return -1;
  }
  
  bytes_written = write(
    fdtarget,
    (void *)buffer,
    2 * shx_file_length                /* convert 16-bit words to 8-bit bytes */
  );                                   /* do not add 4 bytes for EOF          */
  if( bytes_written != (
    2 * shx_file_length
  ) ) {
    perror( "Failure - write from buffer" );
    (void)close( fdtarget );
    free( buffer );
    free( record_offsets );
    free( record_word_sizes );
    return -1;
  }

  (void)close( fdtarget );
  free( buffer );                      /* or manage externally */
  free( record_offsets );
  free( record_word_sizes );
  fprintf( stdout, "%s\n", shx_name );
  
  return 0;  
}
